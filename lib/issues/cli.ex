defmodule Issues.CLI do
  import Issues.TableFormatter, only: [print_table_for_columns: 2]

  @default_count 4

  @moduledoc """
  Handle the command line parsing and the dispatch to the various functions that
  generate a table of the last _n_ issues in a github project.
  """

  @spec main([binary]) :: :ok
  def main(argv) do
    argv
    |> parse_args()
    |> process()
  end

  @doc """
  `argv` can be -h or --help, which returns :help.
    Otherwise it is a github user name, project name, and (optionally)
    the number of entries to format.
  Return a tuple of `{ user, project, count }`, or `:help` if help was given.
  """
  def parse_args(argv) do
    {_, args, _} = OptionParser.parse(argv, strict: [help: :boolean], aliases: [h: :help])

    do_parse_args(args)
  end

  defp do_parse_args([user, project, count]) do
    count = Integer.parse(count)

    case count do
      {count, _} -> {user, project, count}
      :error -> :help
    end
  end

  defp do_parse_args([user, project]), do: {user, project, @default_count}
  defp do_parse_args(_), do: :help

  @spec process({binary, binary, number}) :: :ok | no_return
  def process(:help) do
    IO.puts("usage: issues <user> <project> [ count | #{@default_count} ]")
    System.halt()
  end

  def process({user, project, count}) do
    response = Issues.GithubIssues.fetch(user, project)

    response
    |> decode_response()
    |> sort_into_descending_order()
    |> last(count)
    |> print_table_for_columns(["number", "created_at", "title"])
  end

  defp decode_response({:ok, []}) do
    IO.puts("No issues to display")
    System.halt()
  end

  defp decode_response({:ok, body}), do: body

  defp decode_response({:error, body}) do
    IO.puts("Error fetching from GitHub: #{body["message"]}")
    System.halt(2)
  end

  @spec sort_into_descending_order(list) :: list
  def sort_into_descending_order(issues) do
    Enum.sort(
      issues,
      fn i, j -> i["created_at"] >= j["created_at"] end
    )
  end

  @spec last(list, number) :: list
  def last(list, count) do
    list
    |> Enum.take(count)
    |> Enum.reverse()
  end
end
