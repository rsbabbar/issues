defmodule Issues.TableFormatter do
  import Enum, only: [each: 2, map: 2, map_join: 3, max: 1]

  @spec print_table_for_columns([String.t()], [String.t()]) :: :ok
  def print_table_for_columns(rows, headers) do
    data_by_columns = split_into_columns(rows, headers)
    column_widths = widths_of(data_by_columns)
    format = format_for(column_widths)

    puts_one_line_in_columns(headers, format)
    column_widths |> separator() |> IO.puts()
    puts_in_columns(data_by_columns, format)
  end

  @spec split_into_columns([String.t()], [String.t()]) :: [String.t()]
  def split_into_columns(rows, headers) do
    for header <- headers do
      for row <- rows, do: printable(row[header])
    end
  end

  @spec printable(any) :: String.t()
  def printable(str) when is_binary(str), do: str
  def printable(str), do: to_string(str)

  @spec widths_of([String.t()]) :: [number]
  def widths_of(columns) do
    for column <- columns, do: column |> map(&String.length/1) |> max
  end

  @spec format_for([number]) :: String.t()
  def format_for(column_widths) do
    map_join(column_widths, " | ", fn width -> "~-#{width}s" end) <> "~n"
  end

  @spec separator([number]) :: String.t()
  def separator(column_widths) do
    map_join(column_widths, "-+-", fn width -> List.duplicate("-", width) end)
  end

  @spec puts_in_columns([[String.t()]], String.t()) :: :ok
  def puts_in_columns(data_by_columns, format) do
    data_by_columns
    |> List.zip()
    |> map(&Tuple.to_list/1)
    |> each(&puts_one_line_in_columns(&1, format))
  end

  @spec puts_one_line_in_columns([String.t()], atom | binary | charlist()) :: :ok
  def puts_one_line_in_columns(fields, format) do
    :io.format(format, fields)
  end
end
